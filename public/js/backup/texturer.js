
minCanvasWidth=512;
minCanvasHeight=512;
const bgPatternsImg = ["/img/pattern/gray.png", "/img/pattern/white.png", "/img/pattern/black.png"];

const imageName = "texture_atlas.png";
const jsonName = "texture_atlas.json"

let imgIdCounter=0;
const imageList=[];

const imageListElement = undefined;

const canvas = $("#canvas")[0];
let ctx = undefined;
let patternImg = undefined;
let color=undefined;

let bgSet=undefined;
let highlightIdSet = -1;
// let canvasWidth = undefined;
// let canvasHeight = undefined;
scaleSet = 1;

//used to stablish the pivox x, y
//when an image will be dragged
let selectImgX=0;
let selectImgY=0;
let imgIdSelected=-1;

// let listItemSelected = undefined;

$(document).ready(
    function(){

        // canvasWidth = canvas.width;
        // canvasHeight = canvas.height;

        ctx = canvas.getContext('2d');

        //setting pattern first time
        loadImg( bgPatternsImg[0] ).then(img => {
            patternImg = img;
            setCanvasBg( "pattern" );
        });
        
        setHighlightColor( )

        // $("#imageList").html( itemTemplate() );

        document.getElementById('upload-img').
            addEventListener('change', (event) => {
            const fileList = event.target.files;
            console.log( "setting bg loading imgs: ", fileList.length );

            loadImages( fileList );
            
          });
        
        window.onbeforeunload = function(){
            return 'Are you sure you want to leave?';
        };

        //scale on mouse wheel scroll
        canvas.addEventListener("mousewheel", 
        (e)=>{
            e.preventDefault();
            console.log(`MOUS WHEEL:`, e);
            
            if(e.deltaY < -50 )
            {
                scaleSet += 1;
                if(scaleSet >= 4 )scaleSet = 4;   
            }
            else if( e.deltaY > 50 )
            {
                scaleSet -= 1;
                if(scaleSet <= 1 )scaleSet = 1;
            }

            scaleCanvas( scaleSet );
            $("#scaleSel").val(scaleSet)
        }
        );
        
        //to click on canvas and select images with click
        canvas.addEventListener("mouseup", (e) => {

            const coords = getMouseCoordinates(e);
           
            //check if coordinates are inside image.box measures
            for( img of imageList )
            {
                if( pointCollision( coords.x, coords.y, img.x, img.y, img.w, img.h) )
                {
                    //possible click on an image
                    // console.log(`POINT COL`,img.id)
                    getData(img.id)
                    // toggle(img.id);
                    break;
                }
                // else{
                //     console.log("NO COL")
                // }
            }

            releaseImg(e);

        });

        //for dragging image and change position
        canvas.addEventListener("mousedown", (e) => {
            selectImg(e);
        });
        canvas.addEventListener("mousemove", (e) => {
            dragImg(e);
        });

        
    }
);

/**
 * will set canvas pattern or color
 */
function setCanvasBg(value)
{ 
    
    bgSet = value;
    // console.log( "setCanvasBg::bgSet ", value )
    switch( value )
    {
        
        case "pattern":
        // default:
                ctx.fillStyle = ctx.createPattern(patternImg, "repeat");
            break;
        case "gray": ctx.fillStyle = "#cccccc";
            break;
        case "white": ctx.fillStyle = "#FFFFFF"; 
            break;
        case "black": ctx.fillStyle = "#000000";
            break;
    }

    ctx.fillRect(0, 0, canvas.width, canvas.height);
    
    drawImgs();

    console.log("BG highsligj ", $("#img_sel_id").val())
    showHighlight( parseInt( $("#img_sel_id").val() ) )
    // setHighlightColor( highlightIdSet )
}

function setCanvasWidth(newWidth)
{
    // console.log(newWidth)
    canvas.width = newWidth;
    setCanvasBg( bgSet );
    drawImgs();
}

function setCanvasHeight(newHeight)
{
    // console.log(newHeight)
    canvas.height = newHeight;
    setCanvasBg( bgSet );
    drawImgs();
}

function triggerUploadImgs()
{
    //const iploadImgs = 
    $("#upload-img").trigger('click');
}


/**
 * this load images retrieved from input=file element, 
 * and put those image files in the list
 * @param {} fileList 
 */
function loadImages( fileList )
{
    
    for( const file of fileList )
    {
        if( checkRepeatedImg( file ) )continue;
        const fileReader = new FileReader();

        fileReader.onload = function(e)
        {
            const img = new Image();
            img.src = e.target.result;
            img.onload = () =>
            {

                const imgObj = {
                    "id": ++imgIdCounter,
                    "name": file.name.substring(0, file.name.lastIndexOf('.') ),
                    "label":"",
                    "copies":1,
                    "imageMeasures":{
                    "x": 0,
                    "y": getY( img ),
                    "w": img.width,
                    "h": img.height,
                    "frames": 1,
                    "frameWidth": img.width,
                    "frameHeight": img.height
                    },
                    
                    "img": img
                };
                
                imageList.push(imgObj);

                // checkImgMeasures();
                setCanvasBg( bgSet );
                drawImgs();

                let itmList = "";
                for( const imgData of imageList )
                {
                    itmList += getItemTemplate(imgData);
                }

                $("#imageList").html( itmList );

                //scale canvas when loading images
                //if necessary 
                if( scaleSet !== 1)
                   scaleCanvas( scaleSet )
                
            }
            
        }

        fileReader.readAsDataURL(file);
    }
}


/**
 * this will redraw all the images again in the canvas and will
 * redraw/change the info of the object properties in right section,
 * if a property changes for each object, everything will be redraw again.
 */
function drawImgs()
{
    for( const imgData of imageList )
    {
        // console.log("drawing image ", img)
        ctx.drawImage( imgData.img, imgData.imageMeasures.x, imgData.imageMeasures.y);
    }
}

function getItemTemplate(data)
{
    txt =`<li id="list_item_${data.id}" onclick="getData(${data.id})" class="list-group-item" ><span id="list_item_name_${data.id}" >${data.name}</span>`;  
    txt+=`<button type="button" id="" class="btn btn-danger" style="float:right;" onclick="removeItem(${data.id})" >   `
    txt+=`<span class="glyphicon glyphicon-trash"></span></button></li>`;
    
    return txt;
}

/**
 * returns next Y coordinate position for the new
 * image object
 */
function getY( img )
{

    //get maxY of the current list
    let maxY = 0;
    for( let idx=0; idx < imageList.length ;idx++ )
    {
        let yx = imageList[idx].imageMeasures.y + imageList[idx].imageMeasures.h; 
        if( yx >= maxY ) maxY = yx;
    }
    
    return maxY;
}


function toggle(id)
{
    highlightIdSet = id;
    
    setCanvasBg( bgSet );
    showHighlight(id);
}

function showHighlight()
{
    const id = parseInt( $("#img_sel_id").val() );
    console.log(`HIGHLIGHT SET: `, $("#img_sel_id").val() )

    if( imageList[id-1] )
    {
        // ctx.clearRect(0,0, canvas.width, canvas.height );
        
        ctx.strokeStyle = gridColor;
        ctx.lineWidth = 1;
        const img = imageList[id-1]

        if( img.imageMeasures.w / img.imageMeasures.frameWidth !== 1)
        {
           //show highlight frames
            for( let i=0; i < img.imageMeasures.frames; i++)
            {
                const xx = i * img.imageMeasures.frameWidth;
                ctx.strokeRect( img.imageMeasures.x+xx, img.imageMeasures.y, img.imageMeasures.frameWidth, img.imageMeasures.h );
            }

        }
        else{
            ctx.strokeRect( img.imageMeasures.x, img.imageMeasures.y, img.imageMeasures.w, img.imageMeasures.h );
        }
                
    }

}

async function loadImg( url )
{
    return await new Promise( (resolve)=>{
        const img = new Image();
        img.addEventListener('load', () => { console.log("UNG LOADED"); resolve(img) });
        img.src = url;
    } );

}

function setHighlightColor( )
{
    gridColor = $("#highlightColor").val();
    showHighlight( parseInt( $("#img_sel_id").val() )  );
}


function removeItem(id)
{    
    maxH = 0;
    counter = 0;
    imageList.splice( Number.parseInt(id)-1, 1 );

    for( img of imageList )
    {
        img.id = ++counter;
    }
 
    //after modifiying list redraw all items everywhere
    // checkImgMeasures()//restore canvas width and height if needed
    setCanvasBg( bgSet );
    drawImgs();

    let itmList = "";
    for( const imgData of imageList )
    {
        itmList += getItemTemplate(imgData);
    }

    $("#imageList").html( itmList );

}


function setFrameWidth(val)
{
    const id = parseInt( $("#img_sel_id").val() );
    imageList[ id-1 ].imageMeasures.frameWidth = parseInt(val);

    console.log("frameWidth set:", val)
    console.log("img.frameWidth :", imageList[ id-1 ].imageMeasures.frameWidth)
    

    const frames = 
        Math.trunc( imageList[ id-1 ].imageMeasures.w/imageList[ id-1 ].imageMeasures.frameWidth );
    
    imageList[ id-1 ].imageMeasures.frames = frames; 
    $("#img_sel_f").val(frames);

    console.log("final frames set: ",frames)
    // img_sel_f

    // checkImgMeasures();
    setCanvasBg(bgSet);
    drawImgs();
    showHighlight(id);
}

function setImgX( val )
{
    const id = parseInt( $("#img_sel_id").val() );
    imageList[ id-1 ].imageMeasures.x = parseInt(val);
    // checkImgMeasures();
    setCanvasBg(bgSet);
    drawImgs();
    showHighlight(id);
 
}

function setImgY(val)
{
    const id = parseInt( $("#img_sel_id").val() );
    console.log(`ITEM ID: ${id}`)
    imageList[ id-1 ].imageMeasures.y = parseInt(val);
    // checkImgMeasures()
    setCanvasBg(bgSet);
    drawImgs();
    showHighlight(id);

}


/**
 * if loaded image width and/or height are bigger
 * than canvas, canvas will adjust its width and height
 */
function alignBottom()
{
    console.log("alignBottom")

    let xx=0, xbigger=0;
    let yy=0, ybigger=0;

    let maxW = canvas.width;
    let maxH = canvas.height;

    for( img of imageList )
    {
        //setting new width if image loaded is bigger
        // xx = parseInt(img.x) + parseInt(img.w);
        // if( xx > xbigger) xbigger = xx;
        // if( xx > maxW )maxW = xx;

        // yy += img.h;        
        xx = parseInt(img.imageMeasures.x) + parseInt(img.imageMeasures.w);
        if(xx > xbigger )xbigger = xx;

        yy = parseInt(img.imageMeasures.y) + parseInt(img.imageMeasures.h);
        if(yy > ybigger )ybigger = yy;

    }

    console.log("::: XX "+xx)
    console.log("::: YY "+yy)


        // if( maxW === canvas.width && xbigger < maxW && xbigger > minCanvasWidth )
        // {
        //     // there was no change
        //     console.log(`modificar canvas:`, xbigger)
        //     canvas.width = xbigger;
        //     $("#cvWidth").val(xbigger);
        // }
        // else if( maxW > minCanvasWidth)
        // {
        //     canvas.width = maxW;
        //     $("#cvWidth").val(maxW);
        // } 
        // else if( maxW < minCanvasWidth )canvas.width = minCanvasHeight; 

        if( imageList.length !==0 )
        {
            $("#cvWidth").val(xbigger);
            canvas.width = xbigger;
            $("#cvHeight").val(ybigger);
            canvas.height = ybigger;
        }
        
        setCanvasBg( bgSet )
        drawImgs();
}

function download()
{
    downloadCanvasAsImage();
    downloadJsonData();
}

function downloadCanvasAsImage()
{

    scaleCanvas(1);
    ctx.clearRect(0,0,canvas.width, canvas.height)
    drawImgs();

    let imgUrl = canvas
             .toDataURL("image/png").replace("image/png", "image/octet-stream");
    // window.location.href = imgUrl;
    saveAs( imgUrl, "atlas.png") 

    scaleCanvas( scaleSet );
    setCanvasBg(bgSet);
}

function downloadJsonData()
{
//    const jsonFile = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify( imageList ));
//    window.location.href = jsonFile;

   let jsonFile = new Blob([JSON.stringify( imageList )], 
                    { type: 'application/javascript;charset=utf-8' });

    saveAs( jsonFile, "atlas.json")               
//     const jsonLink = window.URL.createObjectURL( jsonFile ); 
//    window.location = jsonLink;

}

function downloadZip()
{
 const zip = new JSZip();
 zip.file( "atlas.json", JSON.stringify(imageList) );
 zip.file( "atlas.png", ctx.getImageData(0,0,canvas.width, canvas.height).data.buffer );

 zip.generateAsync({type:"blob"}).then( content => 
    {
        location.href="data:application/zip;base64," + content;
        // saveAs( content, "atlas.zip") 
    });
}


function scaleCanvas(val)
{
    scaleSet = val;

    console.log(parseInt( $("#cvWidth").val() ))
    console.log(parseInt( $("#cvHeight").val() ))

        canvas.width = parseInt( $("#cvWidth").val() * val );
        canvas.height = parseInt( $("#cvHeight").val() * val );

    ctx.scale(scaleSet, scaleSet);

    //this will draw images too
    
    setCanvasBg( bgSet );
    // showHighlight( highlightIdSet )

}

/**
 * this will check if current file to add to imageList
 * is already in the list, returns true if so
 */
function checkRepeatedImg( file )
{
    
    for( img of imageList )
    {
        if( file.name === img.name )
        {
            return true;
        }
    }
return false;
}

function setName(val)
{
    const id = parseInt( $("#img_sel_id").val() );
    imageList[ id-1 ].name = val;

    $("#list_item_name_"+id).html(val);
    
}

function setCopies(val)
{
    const id = parseInt( $("#img_sel_id").val() );
    imageList[ id-1 ].copies = parseInt(val);
    // $("#list_item_name_"+id).html(val);   
}

function getMouseCoordinates(e)
{
    const boundingRect = canvas.getBoundingClientRect();
    let eX = e.clientX - boundingRect.left;
    let eY = e.clientY - boundingRect.top;

    // if( scaleSet !== 1 )
    // {
        eX = Math.floor(eX/scaleSet)
        eY = Math.floor(eY/scaleSet)  
    // }

    return {x:eX, y:eY};
}

/**
 * checks whether point (x,y) is inside a box (x2,y2,w2,h2)
 * @param {*} x 
 * @param {*} y 
 * @param {*} x2 
 * @param {*} y2 
 * @param {*} w2 
 * @param {*} h2 
 * @returns 
 */
function pointCollision( x, y,x2,y2,w2,h2 )
{
    return (x >= x2  && x <= x2 + w2 && y >= y2 && y <= y2 + h2 );
}

/**
 * call it during mouse down
 */
function selectImg(e)
{
    
    if( imgIdSelected === -1 )
    {
        const coords = getMouseCoordinates(e);

        for( img of imageList)
        {
            if( pointCollision( coords.x, coords.y,
                img.imageMeasures.x, img.imageMeasures.y, img.imageMeasures.w, img.imageMeasures.h ) )
            {
                selectImgX = coords.x - img.imageMeasures.x;
                selectImgY = coords.y - img.imageMeasures.y;
        
                imgIdSelected = img.id;
                console.log(`img selected ${imgIdSelected} - ${img.name} `)
                break;
            }

        }

       
    }

}


/**
 * call it during mouse move
 */
function dragImg(e)
{
    if( imgIdSelected !== -1 )
    {
        const coords = getMouseCoordinates(e);
        console.log(`IDSEL: ${imgIdSelected} ${coords.x}, ${coords.y}`)
        imageList[ imgIdSelected-1 ].imageMeasures.x = coords.x - selectImgX;
        imageList[ imgIdSelected-1 ].imageMeasures.y = coords.y - selectImgY;
    
        setCanvasBg( bgSet )
        drawImgs();
        // showHighlight( highlightIdSet )
    }

}


/**
 * call it during mouseup
 */
function releaseImg(e)
{
    if( imgIdSelected !== -1 )
    {
        const coords = getMouseCoordinates(e);
        let nX = coords.x - selectImgX;
        let nY = coords.y - selectImgY;

        nX = nX < 0?0:nX;
        nY = nY < 0?0:nY;

        // const id = imgIdSelected-1;
        imageList[ imgIdSelected-1 ].imageMeasures.x = nX;
        imageList[ imgIdSelected-1 ].imageMeasures.y = nY;

        //change x and y on UI
        $(`#item_${imgIdSelected}_x`).val( nX );
        $(`#item_${imgIdSelected}_y`).val( nY );
        
        getData( imgIdSelected )

        // setImgX( imgIdSelected, nX )
        // setImageY( imgIdSelected, nY )
        imgIdSelected = -1;
    }
    
}


function getData(id)
{
    for( img of imageList )
    {
        console.log(`GET DATA ID: ${img.id} - ${id}`)
        if( img.id === id )
        {
            //set correct active list
            for( let i of imageList )
            {
                $("#list_item_"+i.id).removeClass("active");
            }
            console.log("#list_item_"+img.id)
            $("#list_item_"+img.id).addClass("active")

            $("#img_sel_id").val( id );
            $("#img_sel_name").val( img.name );
            $("#img_sel_label").val( img.label );
            $("#img_sel_copies").val( img.copies );
            $("#img_sel_x").val( img.imageMeasures.x );
            $("#img_sel_y").val( img.imageMeasures.y );
            $("#img_sel_w").val( img.imageMeasures.w );
            $("#img_sel_h").val( img.imageMeasures.h );
            $("#img_sel_fw").val( img.imageMeasures.frameWidth );
            $("#img_sel_fh").val( img.imageMeasures.frameHeight );
            $("#img_sel_f").val( img.imageMeasures.frames );



            //hightlist image in canvas
            setCanvasBg(bgSet);
            imgIdSelected = id;
            showHighlight( imgIdSelected );
            imgIdSelected=-1;
            break;
        }
    }
}


function setLabel(val)
{
    const id = parseInt( $("#img_sel_id").val() );
    imageList[ id-1 ].label = val;
}

