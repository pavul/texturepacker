

minCanvasWidth = 640;
minCanvasHeight = 360;
isLoadingProject = false;

const metadata ={
    name:"tilered",
    version:"0.0.0",
    url:"https://gitlab.com/pavul/texturepacker"
}
const bgPatternsImg = ["/img/pattern/gray.png", "/img/pattern/white.png", "/img/pattern/black.png"];

// const imageName = "texture_atlas.png";
const FILE_NAME = "tilerer";

let imgIdCounter = 0;
let layerIdCounter = 0;
let areaIdCounter = 0;

let tilesetList = [];
let layerList = [];
let areaList = [];

// const imageListElement = undefined;

const canvas = $("#canvas")[0];
let ctx = undefined;

const tilesetCanvas = $("#tilesetCanvas")[0];
let tilesetCtx = undefined;

let patternImg = undefined;
let gridColor = undefined;

let tsGridColor = undefined;

let bgSet = undefined;
let highlightIdSet = -1;
// let canvasWidth = undefined;
// let canvasHeight = undefined;
scaleSet = 1;
tilesetScale = 1;

//used to stablish the pivox x, y
//when an image will be dragged
let selectImgX = 0;
let selectImgY = 0;
let imgIdSelected = -1;
let areaIdSelected = -1;

//default grid, rows & cols values
const LYR_DEF_GRIDW = 32;
const LYR_DEF_GRIDH = 32;

let LYR_DEF_ROWS = calculateCells(canvas.width, LYR_DEF_GRIDW);
let LYR_DEF_COLS = calculateCells(canvas.height, LYR_DEF_GRIDH);

let tileSelected = undefined;
let clickPressed = false;

let isGridVisible = true;
let isPainting = true;
let tilePressed = {
    "initX": 0,
    "initY": 0,
    "presed": false
}


let isTilesetGridVisible=true;

modalOpen=false;


$(document).ready(
    function () 
    {

        ctx = canvas.getContext('2d');
        tilesetCtx = tilesetCanvas.getContext('2d');

        //paint tileset canvas in black
        tilesetCtx.fillStyle = "#ccc";
        tilesetCtx.fillRect(0, 0, 128, 128);

        //setting pattern first time
        loadImg(bgPatternsImg[0]).then(img => {
            patternImg = img;
            setCanvasBg("pattern");

            //draw grid here after pattern image is loaded 
            //for first time
            drawGrid(ctx, canvas, layerList[0].gridW, layerList[0].gridH, scaleSet,gridColor);

        });


        //when tilesets are loaded
        document.getElementById('upload-files').
            addEventListener('change', (event) => {
                const fileList = event.target.files;
                console.log("setting bg loading imgs (tilesets): ", fileList.length);
                loadFiles(fileList);
            });

        window.onbeforeunload = function () {
            return 'Are you sure you want to leave?, progress will be lost.';
        };

        window.addEventListener("keyup", (e)=>{
            if( !modalOpen )
            {
            // console.log("calling keyup: "+e.key)
            switch (e.key) {
                case 'b'://select brush
                    setPainting(true);
                    break;
                case 'e'://select eraser
                    setPainting(false);
                    break;
                case 'r':
                    resetLayer();
                    break;    

                    //@todo add shorcut for layer, tileset, area settings? what else
            }
            }
            
        });

        //scale on mouse wheel scroll
        canvas.addEventListener("mousewheel",
            (e) => {
                e.preventDefault();

                scaleSet = getScale(e, scaleSet);
                scaleCanvas(scaleSet);
                $("#scaleSel").val(scaleSet)
            }
        );

        //to click on canvas and select images with click
        canvas.addEventListener("mouseup", (e) => {
            clickPressed = false;
        });

        //for dragging image and change position
        canvas.addEventListener("mousedown", (e) => {
            clickPressed = true;
            putTileOnMap(e);
        });

        canvas.addEventListener("mousemove", (e) => {

            if (clickPressed)
                putTileOnMap(e);
            else {
                setCanvasBg(bgSet);
                dragTile(e);
                drawLayers();
                const layerId = parseInt($("#layerSelected").val())

                if (isGridVisible)
                    drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);
            }

        });

        

        const lyrId = ++layerIdCounter;
        const lyrName = "layer1";
        layerList.push({
            "id": lyrId,
            "name": lyrName,
            "map": initMapArray(canvas, LYR_DEF_GRIDW, LYR_DEF_GRIDH),
            "gridW": LYR_DEF_GRIDW,
            "gridH": LYR_DEF_GRIDH,
            "cols": LYR_DEF_COLS,
            "rows": LYR_DEF_ROWS,
            "visibility": true,
            "opacity": 1
        })

        const tmplt = getLayerItemTemplate(lyrId, lyrName);
        $("#layerList").append(tmplt);
        $(`#layer_itm_list_${lyrId}`).addClass("active")

        tilesetCanvas.addEventListener("mousewheel",
            (e) => {
                e.preventDefault();
                let tilesetId = parseInt($("#tilesetSelected").val());
                if (tilesetId === 0) return;

                // console.log("entering TILESET CANVAS SCALE")
                tilesetScale = getScale(e, tilesetScale);
                scaleTileset();
                $("#tilesetScaleLabel").html(tilesetScale);
            });


        tilesetCanvas.addEventListener("mousemove",
            (e) => {
                if (tilePressed.presed)selectTile(e);
            });

        tilesetCanvas.addEventListener("mouseup",
            (e) => {
                
                selectTile(e);
                tilePressed.presed = false;//this is important to be here below selectTile
            });

        //to get initial x & y coordinates
        tilesetCanvas.addEventListener("mousedown",
            (e) => {
                pressTile(e);
            });

        //load dialog
        $('#basic-modal .basic').click(function (e) {
            $('#basic-modal-content').modal();
            return false;
        });

        setGridColor();

    }//ready end
);

/**
 * will set canvas pattern or color
 */
function setCanvasBg(value) {

    bgSet = value;
    // console.log( "setCanvasBg::bgSet ", value )
    switch (value) {
        case "pattern":
            // default:
            ctx.fillStyle = ctx.createPattern(patternImg, "repeat");
            break;
        case "gray": ctx.fillStyle = "#cccccc";
            break;
        case "white": ctx.fillStyle = "#FFFFFF";
            break;
        case "black": ctx.fillStyle = "#000000";
            break;
    }

    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // setHighlightColor( highlightIdSet )
}

function setCanvasWidth(newWidth) {
    // console.log(newWidth)
    canvas.width = newWidth;

    for (const lyr of layerList) {
        lyr.map = initMapArray(canvas, lyr.gridW, lyr.gridH);
        lyr.cols = calculateCells(canvas.width / scaleSet, lyr.gridW);
    }

    setCanvasBg(bgSet);
    drawLayers();
    if (isGridVisible) {
        const layerId = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);
    }
}

function setCanvasHeight(newHeight) {
    // console.log(newHeight)
    canvas.height = newHeight;

    for (const lyr of layerList) {
        lyr.rows = calculateCells(canvas.height / scaleSet, lyr.gridH);
        lyr.map = initMapArray(canvas, lyr.gridW, lyr.gridH);
    }

    setCanvasBg(bgSet);
    drawLayers();
    if (isGridVisible) {
        const layerId = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);
    }
}

function triggerUploadImgs() {
    //const iploadImgs = 
    // $("#upload-img").trigger('click');
    const fileInput = document.getElementById("upload-files");
    fileInput.click();
}


/**
 * this load images retrieved from input=file element, 
 * and put those image files in the list.
 * Each image here is a tileset
 * @param {} fileList 
 */
async function loadFiles(fileList) {


if(isLoadingProject)
{
    console.log("loading project")

    let jsonData = undefined;
    let pngCounter = 0;
    for (const file of fileList) 
    {
        console.log( file.name )
        if( getFileType( file.name ) === 'json'  )
        {
            console.log("is json")

            jsonData = await new Promise( (res, rej)=>
            {
                const fileReader = new FileReader();
                fileReader.onload = event => { console.log(event.target.result); res(JSON.parse(event.target.result)) }
                fileReader.onerror = error => rej(error)
                fileReader.readAsText(file);
                // fileReader.onload = function(e)
                // {
                //     const content = e.target.result;
                //     console.log("jsonloaded: ", conmtent);
                //     res(JSON.parse( content ));
                // }
            } );

            console.log(jsonData);
            // break;
        }
        else
        {
            pngCounter++;
        }
    }//for
    
    if(jsonData !== undefined)
    {
        tilesetList = jsonData.tilesets;
        layerList = jsonData.layers;
        areaList = jsonData.areas;

        let pngLoaded = 0;
        //adding img elements to tilesets list
        for ( const file of fileList ) 
        {
            // console.log("loading proy img: ",file.name )

            //iterate only for png images
            if( getFileType(file.name) !== "png" )continue;

            const fileReader = new FileReader();
            fileReader.onload = function (e)
            {
                // console.log("loading img: ",file.name )

                const img = new Image();
                img.src = e.target.result;
                img.onload = () => {

                    pngLoaded++;

                    for( tileset of tilesetList )
                    {
                        if( file.name.includes( tileset.name ) )
                        {
                            // console.log( `includes img: ${file.name} - ${tileset.name}` )
                            tileset.img = img;
                        }

                    }

                    //draw layers afer all images are loaded
                    if( pngCounter === pngLoaded )
                    {
                        setCanvasBg(bgSet)
                        drawLayers();
                        selectTileset(1);
                        // changeTsGridColor();//this will redraw tileset
                    }
                    
                }//img load
                
            }
            fileReader.readAsDataURL(file);
        }//for


        //set the UI from loaded data
        //layers
        let txt = "";
        for (lyr of layerList) {
            console.log( `creating layer: ${lyr.name} - ${lyr.id}` );
            txt += getLayerItemTemplate(lyr.id, lyr.name);
        }
        // $("#layerSelected").val(0);
        $("#layerList").html(txt);
        selectLayer(1);
        

        //tilesets
        let tlsetTxt = "";
        for( tlset of tilesetList )
        {
            tlsetTxt+= getTilesetTemplate(tlset.id, tlset.name);
        }

        if(tlsetTxt !== "")
        { 
            $("#tilesetList").html( tlsetTxt );
            $("#tilesetSelected").val(1);
            $("#img_" + 1).addClass("active")  
         }
         //>>>
        
        //areas
        let areaTxt = "";
        console.log("areaTxt")
        console.log(areaTxt)
        for( area of areaList )
        {
            // area.id = areaIdCounter;
            areaTxt += getAreaItemTemplate( area.id, "area"+area.id );
        }

        console.log(areaTxt)
        if(areaTxt !== "")
        {
            $("#areaList").html( areaTxt );
        // areaIdSelected=-1;
        }
        

        // const arId = ++areaIdCounter;
        // const name = `area${arId}`
    
        // areaList.push({
        //     "id": arId,
        //     "name": name,
        //     "label":"",
        //     "x":0,
        //     "y":0,
        //     "w":32,
        //     "h":32,
        //     "color":"#FFDA24",
        //     "visibility": true
        // })
    
        // const txt = getAreaItemTemplate(arId, name);
        // $("#areaList").append(txt);

        //finally draw everything again
        // setCanvasBg(bgSet)
        // drawLayers();
        // if (isGridVisible)
        //     drawGrid(ctx, canvas, lyr.gridW, lyr.gridH, scaleSet,gridColor);

    }


}
else
{
    for (const file of fileList) 
    {
        if (checkRepeatedImg(file)) continue;
        const fileReader = new FileReader();

        fileReader.onload = function (e) 
        {
            const img = new Image();
            img.src = e.target.result;
            img.onload = () => {

                const imgObj =
                {
                    "id": ++imgIdCounter,
                    "name": file.name.substring(0, file.name.lastIndexOf('.')),
                    "gridW": 32,
                    "gridH": 32,
                    "imgW": img.width,
                    "imgH": img.height,
                    "img": img,
                    "imgType":file.name.substring(file.name.lastIndexOf('.'))
                }

                tilesetList.push(imgObj);

                $("#tilesetList").append( getTilesetTemplate(imgObj.id, imgObj.name) );

            }

        }

        fileReader.readAsDataURL(file);
    }

}

    


    isLoadingProject = false; 
}


/**
 * this will redraw all the images again in the canvas and will
 * redraw/change the info of the object properties in right section,
 * if a property changes for each object, everything will be redraw again.
 */
// function drawImgs()
// {
//     for( const imgData of tilesetList )
//     {
//         // console.log("drawing image ", img)
//         ctx.drawImage( imgData.img, imgData.x, imgData.y);
//     }
// }

function getItemTemplate(data) {
    txt = `<li id="list_item_${data.id}" onclick="getData(${data.id})" class="list-group-item" ><span id="list_item_name_${data.id}" >${data.name}</span>  <a href="#" style="float:right;" onclick="removeItem(${data.id})"> <span class="glyphicon glyphicon-trash"></span> </a></li>`;
    return txt;
}

/**
 * returns next Y coordinate position for the new
 * image object
 */
function getY(img) {

    //get maxY of the current list
    let maxY = 0;
    for (let idx = 0; idx < tilesetList.length; idx++) {
        let yx = tilesetList[idx].y + tilesetList[idx].h;
        if (yx >= maxY) maxY = yx;
    }

    return maxY;
}


function toggle(id) {
    highlightIdSet = id;

    setCanvasBg(bgSet);
    //show images here
    changeGridColor();
}

function changeGridColor() {
    const layerId = parseInt($("#layerSelected").val());
    // setCanvasBg(bgSet);
    drawLayers();
    drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);
    }

function changeTsGridColor()
{
    // tsGridColor
    const tilesetId = parseInt($("#tilesetSelected").val());
    // setTilesetBg();
    drawTilesetImage();

    if (tilesetList[tilesetId - 1] !== undefined && isTilesetGridVisible)
        drawGrid(tilesetCtx, tilesetList[tilesetId - 1].img, tilesetList[tilesetId - 1].gridW, tilesetList[tilesetId - 1].gridH, tilesetScale, tsGridColor);

}

async function loadImg(url) {
    return await new Promise((resolve) => {
        const img = new Image();
        img.addEventListener('load', () => { resolve(img) });
        img.src = url;
    });
}

/**
this function sets both grid colors for canvas map and tileset
*/
function setGridColor() {
    gridColor = $("#gridColor").val();
    tsGridColor = $("#tsGridColor").val();
    if (isGridVisible)
        changeGridColor(parseInt($("#img_sel_id").val()));

    if(isTilesetGridVisible)
        changeTsGridColor();    
}

function setTsGridColor()
{
    tsGridColor = $("#tsGridColor").val();
    if (isTilesetGridVisible)
        changeTsGridColor();
}


function removeItem(id) {
    maxH = 0;
    counter = 0;
    tilesetList.splice(Number.parseInt(id) - 1, 1);

    for (img of tilesetList) {
        img.id = ++counter;

        // let hx = img.y + img.h;
        // if( hx >= maxH )
        //     maxH = hx;
    }

    //after modifiying list redraw all items everywhere
    // checkImgMeasures()//restore canvas width and height if needed
    setCanvasBg(bgSet);
    drawImgs();

    let itmList = "";
    for (const imgData of tilesetList) {
        itmList += getItemTemplate(imgData);
    }

    $("#imageList").html(itmList);

}


function setFrameWidth(val) {
    const id = parseInt($("#img_sel_id").val());
    tilesetList[id - 1].frameWidth = parseInt(val);

    console.log("frameWidth set:", val)
    console.log("img.frameWidth :", tilesetList[id - 1].frameWidth)


    const frames = Math.trunc(tilesetList[id - 1].w / tilesetList[id - 1].frameWidth);

    tilesetList[id - 1].frames = frames;
    $("#img_sel_f").val(frames);

    console.log("final frames set: ", frames)
    // img_sel_f

    // checkImgMeasures();
    setCanvasBg(bgSet);
    drawImgs();
    changeGridColor(id);
}

function setImgX(val) {
    const id = parseInt($("#img_sel_id").val());
    tilesetList[id - 1].x = parseInt(val);
    // checkImgMeasures();
    setCanvasBg(bgSet);
    drawImgs();
    changeGridColor();

}

function setImgY(val) {
    const id = parseInt($("#img_sel_id").val());
    console.log(`ITEM ID: ${id}`)
    tilesetList[id - 1].y = parseInt(val);
    // checkImgMeasures()
    setCanvasBg(bgSet);
    drawImgs();
    changeGridColor();

}


/**
 * if loaded image width and/or height are bigger
 * than canvas, canvas will adjust its width and height
 */
function alignBottom() {
    let xx = 0, xbigger = 0;
    let yy = 0;

    let maxW = canvas.width;

    for (img of tilesetList) {
        //setting new width if image loaded is bigger
        xx = parseInt(img.x) + parseInt(img.w);
        if (xx > xbigger) xbigger = xx;
        if (xx > maxW) maxW = xx;

        yy += img.h;
    }

    if (maxW === canvas.width && xbigger < maxW && xbigger > minCanvasWidth) {
        // there was no change
        console.log(`modificar canvas:`, xbigger)
        canvas.width = xbigger;
        $("#cvWidth").val(xbigger);
    }
    else if (maxW > minCanvasWidth) {
        canvas.width = maxW;
        $("#cvWidth").val(maxW);
    }
    else if (maxW < minCanvasWidth) canvas.width = minCanvasHeight;

    canvas.height = yy;
    $("#cvHeight").val(yy);

    if (yy < minCanvasHeight) {
        $("#cvHeight").val(minCanvasHeight);
        canvas.height = minCanvasHeight;
    }
}

// function download() {
//     // downloadCanvasAsImage();
//     // downloadJsonData();

//     downloadProyect();

// }

function downloadCanvasAsImage() {

    scaleCanvas(1);
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    drawLayers();

    let imgUrl = canvas
        .toDataURL("image/png").replace("image/png", "image/octet-stream");

    saveAs(imgUrl, `${FILE_NAME}.png`)

    scaleCanvas(scaleSet);
    setCanvasBg(bgSet);
    drawLayers()

    const layerId = parseInt($("#layerSelected").val());
    if (isGridVisible)
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet, gridColor);


}


/**
 * transforms all the layers, tilesets and areas into a json object
 * @returns 
 */
function getProjectJsonData()
{
    //extracting null values and just leaving existing tiles
    //in a different array of the same layer
    for( lyrIdx in layerList )
    {
        const cleanMap = new Array();

        for( cellIdx in layerList[lyrIdx].map)
        {
            if( layerList[lyrIdx].map[cellIdx] !==null )
               cleanMap.push( layerList[lyrIdx].map[cellIdx] )
        }
        layerList[lyrIdx].cleanMap = cleanMap;
    }
    
    const jsonData = {
        "tilesets":tilesetList,
        "layers": layerList,
        "areas": areaList
    }

    return jsonData;
}


/**
 * this function generates a json files with all the content
 * of layers created.
 * every layer has its own properties:
 * name, cols, rows, tileWidth
 */
function downloadJsonData() {
     
    

    let jsonFile = new Blob([JSON.stringify(getProjectJsonData())],
        { type: 'application/javascript;charset=utf-8' });

    saveAs(jsonFile, `${FILE_NAME}.json`)

}

function downloadZip() {
    const zip = new JSZip();
    zip.file("atlas.json", JSON.stringify(tilesetList));
    zip.file("atlas.png", ctx.getImageData(0, 0, canvas.width, canvas.height).data.buffer);

    zip.generateAsync({ type: "blob" }).then(content => {
        location.href = "data:application/zip;base64," + content;
        // saveAs( content, "atlas.zip") 
    });
}



/**
 * this function is used to convert img.src to bloc, to
 * download the tilesets in a project
 * @param {*} base64Image 
 * @returns 
 */
function convertBase64ToBlob(base64Image) {
    // Split into two parts
    const parts = base64Image.split(';base64,');
  
    // Hold the content type
    const imageType = parts[0].split(':')[1];
  
    // Decode Base64 string
    const decodedData = window.atob(parts[1]);
  
    // Create UNIT8ARRAY of size same as row data length
    const uInt8Array = new Uint8Array(decodedData.length);
  
    // Insert all character code into uInt8Array
    for (let i = 0; i < decodedData.length; ++i) {
      uInt8Array[i] = decodedData.charCodeAt(i);
    }
  
    // Return BLOB image after conversion
    return new Blob([uInt8Array], { type: imageType });
  }



async function downloadProyect()
{
    const zip = new JSZip();

    zip.file("tilered.json", JSON.stringify(getProjectJsonData()));
    
    tilesetList.forEach( tileset =>{
        console.log(tileset.img.src)
        // console.log(tileset.img)

        try{
            zip.file( `${tileset.name}${tileset.imgType}` , convertBase64ToBlob(tileset.img.src) , { type: "blob" });
        }catch(error){
            console.error(error)
        }

    });
    
    // zip.file("atlas.png", ctx.getImageData(0, 0, canvas.width, canvas.height).data.buffer);

    zip.generateAsync({ type: "blob" }).then(content => {
        // location.href = "data:application/zip;base64," + content;
        saveAs( content, "tilered.zip") 
    });

}

function scaleCanvas(val) {
    scaleSet = val;

    canvas.width = parseInt($("#cvWidth").val() * val);
    canvas.height = parseInt($("#cvHeight").val() * val);

    ctx.scale(scaleSet, scaleSet);
    setCanvasBg(bgSet);
    drawLayers();

    const layerId = parseInt($("#layerSelected").val());
    if (isGridVisible)
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);

    // showHighlight( highlightIdSet )
}

function scaleTileset() {
    const tilesetId = parseInt($("#tilesetSelected").val());
    if (tilesetId === 0) return;

    const tls = tilesetList[tilesetId - 1];

    tilesetCanvas.width = tls.img.width * tilesetScale;
    tilesetCanvas.height = tls.img.height * tilesetScale
    tilesetCtx.scale(tilesetScale, tilesetScale);
    setTilesetBg(tilesetId);

    //draw tileset
    tilesetCtx.drawImage(tls.img, 0, 0);

    //draw grid if visible
    if (isTilesetGridVisible)
        drawGrid(tilesetCtx, tls.img, tls.gridW, tls.gridH, tilesetScale,tsGridColor);


}

/**
 * this will check if current file to add to imageList
 * is already in the list, returns true if so
 */
function checkRepeatedImg(file) {

    for (img of tilesetList) {
        if (file.name === img.name) {
            return true;
        }
    }
    return false;
}

function setName(val) {
    const id = parseInt($("#img_sel_id").val());
    tilesetList[id - 1].name = val;

    $("#list_item_name_" + id).html(val);

}

function getMouseCoordinates(e, cnvs) {
    const boundingRect = cnvs.getBoundingClientRect();
    let eX = e.clientX - boundingRect.left;
    let eY = e.clientY - boundingRect.top;

    // eX = Math.trunc(eX/scaleSet)
    // eY = Math.trunc(eY/scaleSet)  

    return { x: eX, y: eY };
}

/**
 * checks whether point (x,y) is inside a box (x2,y2,w2,h2)
 * @param {*} x 
 * @param {*} y 
 * @param {*} x2 
 * @param {*} y2 
 * @param {*} w2 
 * @param {*} h2 
 * @returns 
 */
// function pointCollision( x, y,x2,y2,w2,h2 )
// {
//     return (x >= x2  && x <= x2 + w2 && y >= y2 && y <= y2 + h2 );
// }

function getScale(event, scaleVal) {

    if (event.deltaY < -50) {
        scaleVal += 1;
        if (scaleVal >= 4) scaleVal = 4;
    }
    else if (event.deltaY > 50) {
        scaleVal -= 1;
        if (scaleVal <= 1) scaleVal = 1;
    }
    return scaleVal;
}


function setTilesetBg(id) {

    tilesetCtx.clearRect(0, 0, tilesetList[id - 1].img.width * tilesetScale, tilesetList[id - 1].img.height * tilesetScale);
    tilesetCtx.fillStyle = tilesetCtx.createPattern(patternImg, "repeat");
    tilesetCtx.fillRect(0, 0, tilesetList[id - 1].img.width * tilesetScale, tilesetList[id - 1].img.height * tilesetScale);
}

function selectTileset(id) {
    //get tilesetSelected id
    let pastId = $("#tilesetSelected").val();
    if (pastId !== 0)
        $("#img_" + pastId).removeClass("active")

    $("#tilesetSelected").val(id);
    $("#img_" + id).addClass("active")

    scaleTileset()
    setTilesetBg(id);
    drawTilesetImage();
    drawSelectedTile()

    if (isTilesetGridVisible)
        drawGrid(tilesetCtx,
            tilesetList[id - 1].img,
            tilesetList[id - 1].gridW,
            tilesetList[id - 1].gridH, tilesetScale,tsGridColor);
}

function drawTilesetImage() {
    console.log("drawTilesetImage")
    const id = parseInt($("#tilesetSelected").val());

    if (tilesetList[id - 1] !== undefined)
        tilesetCtx.drawImage(tilesetList[id - 1].img, 0, 0);
}

function drawGrid(cctx, img, gw, gh, scale, color) {
    if (!gw || !gh) return;
    cols = Math.trunc((img.width * scale) / (gw * scale));
    rows = Math.trunc((img.height * scale) / (gh * scale));

    // console.log(`-- ${img.width} - ${img.height}, ${gw}, ${gh}, ${scale}` )
    // console.log(`-- ${cols} , ${rows}` )

    cctx.beginPath();

    if (gw !== 0)
        for (c = 1; c <= cols; c++) {
            cctx.moveTo(c * (gw), 0);
            cctx.lineTo(c * (gw), img.height)
        }

    if (gh !== 0)
        for (r = 1; r <= rows; r++) {
            cctx.moveTo(0, r * (gh));
            cctx.lineTo(img.width, r * (gh));
        }

    cctx.strokeStyle = color;
    cctx.lineWidth = 1;
    cctx.stroke();
}


function setTilesetGridW(val) {
    console.log("Set Grid W")
    const id = parseInt($("#tilesetSelected").val());
    if (id === 0) return;
    tilesetList[id - 1].gridW = val;

    scaleTileset();
    setTilesetBg(id);
    drawTilesetImage();
    drawSelectedTile();

    if(isTilesetGridVisible)
    drawGrid(tilesetCtx, tilesetList[id - 1].img, parseInt($("#tileset_gridw").val()), parseInt($("#tileset_gridh").val()), tilesetScale, tsGridColor)
}

function setTilesetGridH(val) {
    const id = parseInt($("#tilesetSelected").val());
    if (id === 0) return;
    tilesetList[id - 1].gridH = val;

    scaleTileset();
    setTilesetBg(id);
    drawTilesetImage();
    drawSelectedTile();

    if(isTilesetGridVisible)
    drawGrid(tilesetCtx, tilesetList[id - 1].img, parseInt($("#tileset_gridw").val()), parseInt($("#tileset_gridh").val()), tilesetScale, tsGridColor)
}

function layerModalToggle(id) {
    modalOpen=true;
    $('#basic-modal-content').modal();

    const laier = layerList[id - 1];
    $("#modal_layerName").val(laier.name);
    $("#modal_layer_gw").val(laier.gridW);
    $("#modal_layer_gh").val(laier.gridH);
    $("#layerOpacity").val(laier.opacity);
    $("#opacityLabel").html(`Opacity: ${laier.opacity}`);
    $("#layerIdx").val( id-1 );
    // $("#layerUpBtn").prop("id","layerUpBtn_" + (id-1) );
    // $("#layerDownBtn").prop("id", "layerDown_" + (id-1) ); ;
}


function closeLayerModal() {
    $("#modal_layerName").val("");
    $("#modal_layer_gw").val("");
    $("#modal_layer_gh").val("");
    modalOpen=false;
    $.modal.close();
}


function areaModalToggle(id) {
    modalOpen=true;
    $('#area-modal-content').modal();

    // console.log(`AREAID SEL: ${areaIdSelected}`)

    $("#modal_areaLabel").val(areaList[ id-1 ].label);
    $("#modal_area_x").val(areaList[ id-1 ].x)
    $("#modal_area_y").val(areaList[ id-1 ].y) 
    $("#modal_area_w").val(areaList[ id-1 ].w) 
    $("#modal_area_h").val(areaList[ id-1 ].h)
    $("#modal_area_color").val( areaList[ id-1 ].color )
}

function closeAreaModal() {
    
    $("#modal_areaLabel").val("");
    $("#modal_area_x").val("")
    $("#modal_area_y").val("") 
    $("#modal_area_w").val("") 
    $("#modal_area_h").val("")
    modalOpen=false;
    $.modal.close();
}

function setLayerName(val) {
    const id = parseInt($("#layerSelected").val());
    layerList[id - 1].name = val;
    $(`#layer_name_${id}`).html(val);
}

function setLayerGridW(val) {
    const layerId = parseInt($("#layerSelected").val());
    layerList[layerId - 1].gridW = parseInt(val);

    setCanvasBg(bgSet);
    drawLayers();
    if (isGridVisible)
    drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet, gridColor);

}

function setLayerGridH(val) {
    const layerId = parseInt($("#layerSelected").val());
    layerList[layerId - 1].gridH = parseInt(val);

    setCanvasBg(bgSet);
    drawLayers();
    if (isGridVisible)
    drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet, gridColor);

}


function tilesetModalToggle(id) {
    modalOpen=true;
    $('#tileset-modal-content').modal();

    const tileset = tilesetList[id - 1];
    $("#modal_tilesetName").val(tileset.name);
    $("#modal_tileset_gw").val(tileset.gridW);
    $("#modal_tileset_gh").val(tileset.gridH);
}

function closeTilesetModal() {
    $("#modal_tilesetName").val("");
    $("#modal_tileset_gw").val("");
    $("#modal_tileset_gh").val("");
    modalOpen=false;
    $.modal.close();
}

function setTilesetName(val) {
    const id = parseInt($("#tilesetSelected").val());
    tilesetList[id - 1].name = val;
    $(`#tileset_name_${id}`).html(tilesetList[id - 1].name);
}

function setTilesetGridW(val) {
    const id = parseInt($("#tilesetSelected").val());
    tilesetList[id - 1].gridW = parseInt(val);

    // const w = tilesetList[id-1].img.width;
    // const h = tilesetList[id-1].img.height;
    // tilesetCanvas.width = w * tilesetScale;
    // tilesetCanvas.height = h * tilesetScale;
    scaleTileset();
    setTilesetBg(id);
    drawTilesetImage();

    if (isTilesetGridVisible)
        drawGrid(tilesetCtx, tilesetList[id - 1].img, tilesetList[id - 1].gridW, tilesetList[id - 1].gridH, tilesetScale, tsGridColor);

}

function setTilesetGridH(val) {
    const id = parseInt($("#tilesetSelected").val());
    tilesetList[id - 1].gridH = parseInt(val);

    // const w = tilesetList[id-1].img.width;
    // const h = tilesetList[id-1].img.height;
    // tilesetCanvas.width = w * tilesetScale;
    // tilesetCanvas.height = h * tilesetScale;
    scaleTileset();
    setTilesetBg(id);
    drawTilesetImage();

    if (isTilesetGridVisible)
        drawGrid(tilesetCtx, tilesetList[id - 1].img, tilesetList[id - 1].gridW, tilesetList[id - 1].gridH, tilesetScale, tsGridColor);

}

/**
 * this function will select the portion of tileset
 * that will be set later on the map at specific selected layer
 * this is not selectTileset
 */
function selectTile(e) {
    const tilesetId = parseInt($("#tilesetSelected").val());
    if (tilesetId === 0) return;

    const gw = tilesetList[tilesetId - 1].gridW;
    const gh = tilesetList[tilesetId - 1].gridH;

    if (gw === 0 | gh === 0) return;


    //check for initial x & y
    if (tilePressed.presed) {
        // console.log("ENTRA A TILE PRESSED")
        const cols = calculateCells(tilesetCanvas.width / tilesetScale, gw);
        // const rows = calculateCells(tilesetList[ tilesetId-1].imgH,gh);

        const initX = Math.floor((tilePressed.x / tilesetScale) / gw)//-1;
        const initY = Math.floor((tilePressed.y / tilesetScale) / gh)//-1;
        const initIdx = (initY * cols) + initX;

        const endCoords = getMouseCoordinates(e, tilesetCanvas);
        const endX = Math.trunc((endCoords.x / tilesetScale) / gw)//-1;
        const endY = Math.trunc((endCoords.y / tilesetScale) / gh)//-1;
        const endIdx = (endY * cols) + endX;

        // console.log(`iX ${initX} iY ${initY} cols: ${cols} initIdx ${initIdx}`)
        // console.log(`eX ${endX} eY ${endY} cols: ${cols} endIdx ${endIdx}`)

        // console.log(`XX ${(endX - initX)} YY ${(endY - initY)}` )
        // console.log(`XX ${((endX - initX) * gw)+gw} YY ${((endY - initY) * gh)+gh}` )
        // let newW = Math.ceil((endCoords.x - tilePressed.x) / gw); 
        // let newH = Math.ceil((endCoords.y - tilePressed.y) / gh);

        if (initIdx === endIdx) 
        {
            console.log("ENTRA A INDX")
            //draw only one tile
            tileSelected = {
                "tilesetId": tilesetId - 1,
                "tileset": tilesetList[tilesetId - 1].name,
                "srcX": initX * gw,
                "srcY": initY * gh,
                "tileW": gw,
                "tileH": gh,
                "index": initIdx //tileset index
            }
        }
        else 
        {

            // console.log(`tilePressed: ${JSON.stringify(tilePressed)}`)
            // console.log(`endCoords: ${JSON.stringify(endCoords)}`)

            // console.log(`ex:${endCoords.x} - tpX:${tilePressed.x} / 32 = ${(endCoords.x - tilePressed.x) / gw}`)
            // console.log(`ex:${endCoords.y} - tpX:${tilePressed.y} / 32 = ${(endCoords.y - tilePressed.y) / gh}`)

            // const endCoords = getMouseCoordinates(e, tilesetCanvas);
            // const endX = Math.trunc((endCoords.x / tilesetScale) / gw)//-1;
            // const endY = Math.trunc((endCoords.y / tilesetScale) / gh)//-1;
            // const endIdx = (endY * cols) + endX;
            
            // tilePressed.x = Math.trunc(tilePressed.x / gw) * gw
            // tilePressed.y = Math.trunc(tilePressed.y / gh) * gh

            // console.log( `eX:${Math.trunc(endCoords.x) } - iX:${Math.trunc(tilePressed.x)} / scale: ${tilesetScale} `)
            // console.log( `eY:${Math.trunc(endCoords.y)} - iY:${Math.trunc(tilePressed.y)} / scale: ${tilesetScale} `)
            
            //this works fine, does not fill up incomplete tiles
            // let newW = Math.ceil( ( (endCoords.x - tilePressed.x) / tilesetScale ) / gw);
            // let newH = Math.ceil( ( (endCoords.y - tilePressed.y) / tilesetScale ) / gh);


/**
 *      let iniX = Math.floor( (Math.trunc(coords.x) / tilesetScale ) / gw );
        let iniY = Math.floor( (Math.trunc(coords.y) / tilesetScale ) / gh );

        console.log(` iniX: ${iniX} * gw: `)
        console.log(` iniY: ${iniY} * gw: `)

        tilePressed.x = iniX * gw;
        tilePressed.y = iniY * gh;
 */

            let endX = Math.ceil( (Math.trunc(endCoords.x) / tilesetScale ) / gw );
            let endY = Math.ceil( (Math.trunc(endCoords.y) / tilesetScale ) / gw );

            endX *= gw;
            endY *= gh;

            console.log(`ex:${endX} - tp.x:${ tilePressed.x} = ${ endX - tilePressed.x }`)
            console.log(`ex:${endY} - tp.x:${ tilePressed.y} = ${ endY - tilePressed.y }`)

            let newW = endX - tilePressed.x;
            let newH = endY - tilePressed.y;

            // console.log(`ENTRA A MULTITILE ${newW} - ${newH}`)

            // newW = newW === 0 ? gw : newW * gw;
            // newH = newH === 0 ? gh : newH * gh;

            //multiple tiles selected
            // const newWidth = 
            // const newHeight =
            tileSelected = {
                "tilesetId": tilesetId - 1,
                "tileset": tilesetList[tilesetId - 1].name,
                "srcX": tilePressed.x,
                "srcY": tilePressed.y,
                "tileW": newW,// ( endX * gw ),
                "tileH": newH,//( endY * gh ),
                "index": initIdx //tileset index
            }
            // console.log(JSON.stringify(tileSelected))
        }

    }

    setTilesetBg(tilesetId);
    drawTilesetImage();
    drawSelectedTile();

    if (tilesetList[tilesetId - 1] !== undefined && isTilesetGridVisible)
        drawGrid(tilesetCtx, tilesetList[tilesetId - 1].img, tilesetList[tilesetId - 1].gridW, tilesetList[tilesetId - 1].gridH, tilesetScale, tsGridColor);
}

/**
 * this will get initial x * y coordinates
 * @param e 
 */
function pressTile(e) {
    const coords = getMouseCoordinates(e, tilesetCanvas);

    if (!tilePressed.presed) {
        tilePressed.presed = true;

        const tilesetId = parseInt($("#tilesetSelected").val());
        if (tilesetId === 0) return;
    
        const gw = tilesetList[tilesetId - 1].gridW;
        const gh = tilesetList[tilesetId - 1].gridH;

        //@todo check here the tile clicked so we can fix x & y since its initial position
        //and not where click.X and click.Y were made
        // tilePressed.x = coords.x;
        // tilePressed.y = coords.y;
        let iniX = Math.floor( (Math.trunc(coords.x) / tilesetScale ) / gw );
        let iniY = Math.floor( (Math.trunc(coords.y) / tilesetScale ) / gh );

        console.log(` iniX: ${iniX} * gw: `)
        console.log(` iniY: ${iniY} * gw: `)

        tilePressed.x = iniX * gw;
        tilePressed.y = iniY * gh;

        
    }

}

/**
 * this will draw a little rectangle of the piece of tile
 * that was selected
 */
function drawSelectedTile() {
    if (tileSelected !== undefined) {
        tilesetCtx.save();
        tilesetCtx.fillStyle = tsGridColor;
        tilesetCtx.globalAlpha = 0.4;
        tilesetCtx.fillRect(tileSelected.srcX,
            tileSelected.srcY,
            tileSelected.tileW,
            tileSelected.tileH);
        tilesetCtx.restore();
    }
}

function dragTile(e) {
    if (tileSelected !== undefined) {
        const layerId = parseInt($("#layerSelected").val());
        const tilesetId = parseInt($("#tilesetSelected").val());

        const gw = layerList[layerId - 1].gridW / 2;
        const gh = layerList[layerId - 1].gridH / 2;

        const coords = getMouseCoordinates(e, canvas);

        ctx.save()
        ctx.globalAlpha = 0.6;
        ctx.drawImage(tilesetList[tilesetId - 1].img,
            tileSelected.srcX, tileSelected.srcY, tileSelected.tileW, tileSelected.tileH,
            (coords.x / scaleSet) - gw, (coords.y / scaleSet) - gh, tileSelected.tileW, tileSelected.tileH);
        ctx.restore();

    }
}

/**
 * this will be used to calculate rows and cols
 */
function calculateCells(width, cellWidth) {
    return Math.ceil(width / cellWidth);
}

function getCellData(e, tileWidth, tileHeight, cnvs, scale) {
    const cols = calculateCells(cnvs.width / scale, tileWidth);
    // const rows = calculateCells(tilesetList[ tilesetId-1].imgH,gh);

    const coords = getMouseCoordinates(e, cnvs);

    const xx = Math.trunc((coords.x / scale) / tileWidth)//-1;
    const yy = Math.trunc((coords.y / scale) / tileHeight)//-1;
    const index = (yy * cols) + xx;

    return {
        "xx": xx,
        "yy": yy,
        "index": index
    }

}

function putTileOnMap(e) {

    if (!tileSelected) return;

    const layerId = parseInt($("#layerSelected").val());

    //putting tiles while layer is invisible NOT ALLOWED!
    if (!layerList[layerId - 1].visibility) return;

    const gw = layerList[layerId - 1].gridW;
    const gh = layerList[layerId - 1].gridH;


    const cd = getCellData(e, gw, gh, canvas, scaleSet);

    // console.log(`GW: ${gw}  GH:${gh}  - canvasW: ${canvas.width} - scale: ${scaleSet}`)
    // console.log(` putTileOnMap::getCellData : ${JSON.stringify(cd)}`)
    // console.log(`x: `,cd.xx * gw)
    // console.log(`y: ${cd.yy} - ${gh}`)

    if (isPainting) {
        if (!layerList[layerId - 1].map[cd.index]) {
            const xx = cd.xx * gw;
            const yy = cd.yy * gh;

            layerList[layerId - 1].map[cd.index] = {
                "x":xx,
                "y":yy,
                "tilesetId": tileSelected.tilesetId,
                "tileset": tileSelected.tileset,
                "srcX": tileSelected.srcX, //tileset srcX
                "srcY": tileSelected.srcY, //tileset srcY
                "tileW": tileSelected.tileW,
                "tileH": tileSelected.tileH,
                "index": tileSelected.index, //tileset index
                "dstX": xx, //x position of tile in layer( used to draw in layer canvas)
                "dstY": yy //y position of tile in layer( used to draw in layer canvas)
            };
            console.log(layerList[layerId - 1].map[cd.index])

        }
        else if (layerList[layerId - 1].map[cd.index].index !== tileSelected.index) {
            // layerList[layerId-1].map[ cd.index ].tile = tileSelected;
            layerList[layerId - 1].map[cd.index].index = tileSelected.index;
            layerList[layerId - 1].map[cd.index].srcX = tileSelected.srcX;
            layerList[layerId - 1].map[cd.index].srcY = tileSelected.srcY;
        }
    }
    else if (layerList[layerId - 1].map[cd.index] !== undefined) {
        layerList[layerId - 1].map[cd.index] = undefined;
    }

    //redraw canvas with tile on it 
    //set canvas bg
    //draw layers of tiles on canvas
    setCanvasBg(bgSet);
    drawLayers();

    if (isGridVisible)
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet, gridColor);

}


function initMapArray(cnvs, gridW, gridH) {
    const cols = calculateCells(cnvs.width, gridW)
    const rows = calculateCells(cnvs.height, gridH)

    let arr = new Array(cols * rows);
    return arr;

}

/**
 * draws every layer and after that draws the areas if visible
 */
function drawLayers() {

    for (let i = 0; i < layerList.length; i++) 
    {
        if ( layerList[i].visibility )
        {
            for (let tileIdx = 0; tileIdx < layerList[i].map.length; tileIdx++) {
                const tile = layerList[i].map[tileIdx];
                if (!tile) continue;
                else {
                    // const tile = cell.tile;
                    // console.log(`-- DRAWLAYER ${tileIdx} : ${JSON.stringify(tile) }`)
                    ctx.save();
                    ctx.globalAlpha = layerList[i].opacity;
                    ctx.drawImage(tilesetList[tile.tilesetId].img, tile.srcX, tile.srcY, tile.tileW, tile.tileH, tile.dstX, tile.dstY, tile.tileW, tile.tileH);
                    ctx.restore();
                }
            }
        }
            
    }
    //draw here areas
    for(let i=0; i<areaList.length; i++)
    {
        const area = areaList[i]; 
        if(area.visibility)
        {
            ctx.strokeStyle = areaList[i].color;
            ctx.strokeRect(area.x, area.y, area.w, area.h);
        }
    }

}


function changeGridVisibility() {
    isGridVisible = !isGridVisible;

    if (isGridVisible)
        $(`#showGrid`).prop("class", "btn btn-primary");
    else
        $(`#showGrid`).prop("class", "btn btn-secondary");

    const layerId = parseInt($("#layerSelected").val());
    // const tilesetId = parseInt($("#tilesetSelected").val());

    if (isGridVisible) {

        // setCanvasBg(bgSet);
        drawLayers();
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);

        // if(tilesetId!==0)
        //    setTilesetBg(tilesetId);

        // drawTilesetImage();
        // drawSelectedTile();

    }
    else {
        setCanvasBg(bgSet)
        drawLayers();
    }

}

function changeTilesetGridVisibility()
{
    isTilesetGridVisible = !isTilesetGridVisible;

    if (isTilesetGridVisible)
        $(`#showTilesetGrid`).prop("class", "btn btn-primary");
    else
        $(`#showTilesetGrid`).prop("class", "btn btn-secondary");

        const tilesetId = parseInt($("#tilesetSelected").val());

    if (isTilesetGridVisible) {

        if(tilesetId!==0)
           setTilesetBg(tilesetId);

        drawTilesetImage();
        drawSelectedTile();

        if (tilesetList[tilesetId - 1] !== undefined && isTilesetGridVisible)
            drawGrid(tilesetCtx, tilesetList[tilesetId - 1].img, tilesetList[tilesetId - 1].gridW, tilesetList[tilesetId - 1].gridH, tilesetScale, tsGridColor);

    }
    else {
        drawTilesetImage();
    }
}



function changeBg(val) {
    setCanvasBg(val);
    drawLayers();

    if (isGridVisible) {
        const layerId = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);
    }

}

function removeLayer() {
    const layerId = parseInt($("#layerSelected").val());
    if (layerList.length == 1) {
        alert(`layer ${layerList[layerId - 1].name} cannot be removed cause there must be at least 1 layer.`)
    }
    else {
        layerIdCounter = 0;
        //removing layer from array
        layerList.splice(layerId - 1, 1);

        let txt = "";
        for (lyr of layerList) {
            const lyrId = ++layerIdCounter;
            lyr.id = lyrId;
            txt += getLayerItemTemplate(lyrId, lyr.name);
        }

        $("#layerList").html(txt);

    }
    modalOpen=false;
    $.modal.close();

    setCanvasBg(bgSet);
    drawLayers();
    if (isGridVisible)
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);

}

function addLayer() {
    const lyrId = ++layerIdCounter;
    const name = `layer${lyrId}`

    layerList.push({
        "id": lyrId,
        "name": name,
        "map": initMapArray(canvas, LYR_DEF_GRIDW, LYR_DEF_GRIDH),
        "gridW": LYR_DEF_GRIDW,
        "gridH": LYR_DEF_GRIDH,
        "cols": LYR_DEF_COLS,
        "rows": LYR_DEF_ROWS,
        "visibility": true,
        "opacity": 1
    })

    const txt = getLayerItemTemplate(lyrId, name);
    $("#layerList").append(txt);
}


function getLayerItemTemplate(id, name) {
    let txt = "";
    txt += `<li class="list-group-item" id="layer_itm_list_${id}" onclick="selectLayer(${id})" >`;
    txt += `<input class="form-check-input" type="checkbox" id="cb_layer_${id}" checked onchange="changeLayerVisibility(${id})" />`;
    txt += `<span id="layer_name_${id}">${name}</span>`;
    txt += `<button type="button" id="layerSettings_${id}" class="btn btn-danger" style="float:right;" onclick="layerModalToggle(${id})" >`;
    txt += `<span class="glyphicon glyphicon-cog"></span></button>`;
    txt += `</li>`;
    return txt;
}

function getAreaItemTemplate(id, name) {
    let txt = "";
    txt += `<li class="list-group-item" id="area_itm_list_${id}" onclick="selectArea(${id})" >`;
    txt += `<input class="form-check-input" type="checkbox" id="cb_area_${id}" checked onchange="changeAreaVisibility(${id})" />`;
    txt += `<span id="area_name_${id}">${name}</span>`;
    txt += `<button type="button" id="areaSettings_${id}" class="btn btn-danger" style="float:right;" onclick="areaModalToggle(${id})" >`;
    txt += `<span class="glyphicon glyphicon-cog"></span></button>`;
    txt += `</li>`;
    return txt;
}

function selectLayer(id) {
    //get tilesetSelected id
    let pastId = $("#layerSelected").val();
    if (pastId !== 0)
        $("#layer_itm_list_" + pastId).removeClass("active")

    $("#layerSelected").val(id);
    $("#layer_itm_list_" + id).addClass("active");
}


function setPainting(paint) {
    isPainting = paint;

    if (isPainting) {
        $("#paintBtn").prop("class", "btn btn-warning")
        $("#eraserBtn").prop("class", "btn btn-secondary")
    }
    else {
        $("#paintBtn").prop("class", "btn btn-secondary")
        $("#eraserBtn").prop("class", "btn btn-warning")
    }
}

function changeLayerVisibility(id) {
    layerList[id - 1].visibility = !layerList[id - 1].visibility;

    setCanvasBg(bgSet);
    drawLayers();
    if (isGridVisible) {
        const layerId = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, layerList[layerId - 1].gridW, layerList[layerId - 1].gridH, scaleSet,gridColor);
    }

}


function resetLayer() {
    const layerId = parseInt($("#layerSelected").val())
    const lyr = layerList[layerId - 1];
    lyr.map = initMapArray(canvas, lyr.gridW, lyr.gridH);

    setCanvasBg(bgSet)
    drawLayers();

    if (isGridVisible)
        drawGrid(ctx, canvas, lyr.gridW, lyr.gridH, scaleSet,gridColor);
}


function setLayerOpacity(val) {
    const layerId = parseInt($("#layerSelected").val());

    layerList[layerId - 1].opacity = parseFloat(val);
    // $("#layerOpacity").val()
    $("#opacityLabel").html(`Opacity: ${val}`);
}


function addArea()
{
    const arId = ++areaIdCounter;
    const name = `area${arId}`

    areaList.push({
        "id": arId,
        "name": name,
        "label":"",
        "x":0,
        "y":0,
        "w":32,
        "h":32,
        "color":"#FFDA24",
        "visibility": true
    })

    const txt = getAreaItemTemplate(arId, name);
    $("#areaList").append(txt);
}

function selectArea(id)
{
    $("#area_itm_list_"+areaIdSelected).removeClass("active");

    areaIdSelected = id;
    $("#area_itm_list_"+id).addClass("active");
}

function updateAreaData()
{   
    areaList[ areaIdSelected-1 ].label = $("#modal_areaLabel").val();
    areaList[ areaIdSelected-1 ].x = parseInt($("#modal_area_x").val())
    areaList[ areaIdSelected-1 ].y = parseInt($("#modal_area_y").val())
    areaList[ areaIdSelected-1 ].w = parseInt($("#modal_area_w").val())
    areaList[ areaIdSelected-1 ].h = parseInt($("#modal_area_h").val())
    areaList[ areaIdSelected-1 ].color = $("#modal_area_color").val()

    setCanvasBg(bgSet)
    drawLayers();

    if (isGridVisible)
    {
        const lyr = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, lyr.gridW, lyr.gridH, scaleSet,gridColor);
    }
}

function changeAreaVisibility(id)
{
    const isCheked = $("#cb_area_"+id).prop("checked");
    areaList[id-1].visibility = isCheked;

    setCanvasBg(bgSet)
    drawLayers();

    if (isGridVisible)
    {
        const lyr = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, lyr.gridW, lyr.gridH, scaleSet,gridColor);
    }
        
}


function removeArea()
{    
    areaIdCounter = 0;
    areaList.splice( Number.parseInt(areaIdSelected)-1, 1 );

    let areaTxt = "";

    for( area of areaList )
    {
        area.id = ++areaIdCounter;
        areaTxt += getAreaItemTemplate( area.id, "area"+area.id );
    }
    $("#areaList").html( areaTxt );
    areaIdSelected=-1;


    setCanvasBg(bgSet)
    drawLayers();

    if (isGridVisible)
    {
        const lyr = parseInt($("#layerSelected").val());
        drawGrid(ctx, canvas, lyr.gridW, lyr.gridH, scaleSet,gridColor);
    }

    modalOpen=false;
    $.modal.close();
}


function removeTileset()
{    
    imgIdCounter = 0;
    const tilesetId = parseInt($("#tilesetSelected").val());
    tilesetList.splice( Number.parseInt(tilesetId)-1, 1 );

    let tlsetTxt = "";

    for( tlset of tilesetList )
    {
        tlset.id = ++imgIdCounter;
        tlsetTxt+= getTilesetTemplate(tlset.id, tlset.name);

        // area.id = ++areaIdCounter;
        // areaTxt += getAreaItemTemplate( area.id, "area"+area.id );
    }
    $("#tilesetList").html( tlsetTxt );
    $("#tilesetSelected").val(0);

    tilesetCanvas.width=128;
    tilesetCanvas.height=128;
    tilesetCtx.fillStyle="#ccc";
    tilesetCtx.fillRect(0,0,128,128);
    modalOpen=false;
    $.modal.close();
}


function getTilesetTemplate(id, name)
{
    let txt = `<li class="list-group-item" onclick="selectTileset(${id})" id="img_${id}">`;
        txt += `<span id="tileset_name_${id}" >${name}</span><button type="button" id="" class="btn btn-danger" style="float:right;" onclick="tilesetModalToggle(${id})" >`;
        txt += `<span class="glyphicon glyphicon-cog"></span></button></li>`;
return txt;
}


            /**
 * this function will move layer up or down 
 * but only if the index of the item to move is not 
 * te first or the last
 */
            function moveLayer( dir )
            {
                let idx = parseInt($("#layerIdx").val()); 
            
                    itemToMove = layerList.splice( idx, 1 )[0];
                    if(dir === "up" && idx > 0 )
                    {
                        idx = idx-1
                        // if(idx >= layerList.length )idx=layerList.length;
                    }
                    else if( dir === "down" && idx < layerList.length )
                    {
                        idx = idx+1;
                        // if(idx <= 0 )idx=0;
                    }
                        
                    // console.log(`adding idx: ${idx}`, itemToMove.name );
                    layerList.splice( idx, 0, itemToMove );
                    console.log(layerList)

                    $("#layerIdx").val(idx);
                    
                    //list have been updated, redraw layer list
                    // console.log("after moving:")
                    let layerListTemplate = "";
                    for( const i in layerList)
                    {
                        const lyr = layerList[i];
                        // console.log(": ", lyr.name)
                        layerListTemplate+= getLayerItemTemplate( parseInt(i)+1, lyr.name );
                    }

                    $("#layerList").html( layerListTemplate );

                
                    setCanvasBg(bgSet)
                    drawLayers();
                
                    if (isGridVisible)
                    {
                        const lyr = parseInt($("#layerSelected").val());
                        drawGrid(ctx, canvas, lyr.gridW, lyr.gridH, scaleSet,gridColor);
                    }
                        
            }

/**
 * this function will load the content of a tilered file and will create the json with that
 */
function loadTileredJsonFile()
{

}

/**
 * function to trigger load a project from a previously downloaded one
 */
function loadProject()
{

    const res = confirm("if you load a project content will be reset, are you sure?")
    if(res)
    {
        isLoadingProject = true;
        // const fileInput = document.getElementById("upload-files");
        // fileInput.click();
        triggerUploadImgs();
    }
    
}

/**
 * to obtain mime type/extension from uploaded files
 * @param {*} fileName 
 * @returns 
 */
function getFileType(fileName) 
{
    var fileExtension = fileName.split('.').pop().toLowerCase();
    if (fileExtension === 'json') {
      return 'json';
    } else if (fileExtension === 'png') {
      return 'png';
    } else {
      return undefined;
    }
}