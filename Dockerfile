FROM ubuntu:22.04
# RUN npm install -g npm@8.19.1
# RUN npm --version

ARG NODE_VERSION=v16.17.0
ARG NODE_PLATFORM=linux-x64
ENV NODE_PATH=node-$NODE_VERSION-$NODE_PLATFORM


# gsutil needs python3
RUN  apt-get update \
  && apt-get install -yq apt-transport-https ca-certificates gnupg python3 \
  && rm -rf /var/lib/apt/lists/*

#ADD gsutil.tar.gz /

# Add Node.js
ADD $NODE_PATH.tar.xz /usr/

# Add path for Node and gsutil
ENV PATH="/usr/$NODE_PATH/bin:${PATH}"

RUN mkdir app

#copying app needed files to root folder
# COPY package.json package-lock.json app/
COPY package.json app/

COPY dist /app/dist

WORKDIR app/

# RUN pwd
# RUN ls -l

#download all used node dependencies 
# RUN npm install --production

RUN npm install --omit=dev

#uncomment this line if swagger docs should be updated
#RUN npm run swaggerbuild 

#config gsutil stand alone
#ADD build-arg here...
#RUN echo '[GoogleCompute]\nservice_account = default' > /etc/boto.cfg

#RUN gsutil version -l

#create dataserv name and group for hybrid containers
#RUN groupadd -r dataserv && useradd --no-log-init --uid 1200 -r -g dataserv dataserv

#start app
CMD [ "npm","run","start" ]

