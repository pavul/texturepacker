### Bitless Texture Packer
###### this is a set of 2 tools to help to create:
- Textured: creates Atlass of sprites for your games in a way you can sort or rearrange
- Tilered: help with the creation of leves using tiles


## Before Start
1. you need to have npm and node installed in your computer ( verify you have correct versions )
2. then you can execute command ```npm install``` this will install all devDependencies and dependencies needed (including typescript)
3. download or clone this repository from master branch
4. open repo or add it to your workspace in visual studio code ( you need this installed also )

## To Start This App
- execute ```npm run build``` command if you are in linux/mac or ```npm run build.win``` if you are in windows after that execute:
- ```npm run start``` this will start an http-server where game is hosted
- enter url provided for ```npm run start``` command (http://localhost:3000)

- you can also use ``` npm run reset ``` this command will build and start the server

## Branches Content
- **master** this is the only branch at the moment, where all code is ready for usage

## NOTES:
- if you are in windows, install node js, npm, and typescript, build it and run it in a server you like, it should not be difficult.
