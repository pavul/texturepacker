
import { Get, Post, Controller, Render, Req, Res, Session } from '@nestjs/common';
// import {Request, Response} from "express";
// import { EmailUtil } from '../util/EmailUtil';
// import { InventoryService } from '../services/InventoryService';
// import { MovingServicesService } from '../services/MovingServicesService';
// import { AccessService } from '../services/AccessService';



@Controller()
export class SiteController
{

    constructor(){
      
      //, private invServ:InventoryService, private accessServ:AccessService){
    
    }

    /**
     * home page
     * @returns 
     */
    @Get(['','textured'])
    @Render('textured_template')
    home(){
        return { 
            // "getpartial": ()=>"dashboard",
            // message: 'this is the message from controller!' 
        };
    }

    @Get(['tilered'])
    @Render('tilered_template')
    tilered(){
        return { 
            // "getpartial": ()=>"dashboard",
            // message: 'this is the message from controller!'
         };
    }

    

}