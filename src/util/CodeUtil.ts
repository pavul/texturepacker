/**
 * this provides some utility functions for general purposes
 */
 export class CodeUtil {
  

  //   /**
  //    * this function returns expiration time usually used 
  //    * for expiration of JWT
  //    * @param expirationHour accepts numbers of ours to expire by default 6
  //    */
  //   static getExpirationTime(expirationHour?: number) {
  //     const hour: number = expirationHour ? expirationHour! : 6;
  
  //     const today = new Date();
  //     today.setHours(today.getHours() + hour);
  //     return today;
  //   }
    
  
  //   /**
  //    * this will return a 6 digit code used when the player
  //    * logs into a game and create a session
  //    */
  // static getDigitRandom(length?:number):number
  // {
  //   //another 100000 + Math.floor(Math.random() * 900000);
  //   length = length?length:6;
  //   return Math.floor( Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1) );
  // }
  
  
  // // this will generate a 10 digit alphanumeric code to use them
  // // as secret, this secret will be used to create JWT for players
  // static getSecret(length?:number):string
  // {
  //   length = length?length:10;
  //   return Array(length+1).join((Math.random().toString( 36 )+'00000000000000000').slice( 2, 18 ) ).slice(0, length);
  //   // return "a";
  // }
  
  
  // /**
  //  * this check if iat has expired
  //  * @param iat 
  //  * @returns 
  //  */
  // static checkCodeExpirationTime( iat:number ):boolean
  // {
  //   const hourInMillis = 1000 * 60 * 60;
  
  //   const now:number = new Date().getTime();
  //   return ( iat > now + hourInMillis || now > iat + hourInMillis );
  // }
  
  
  }
  