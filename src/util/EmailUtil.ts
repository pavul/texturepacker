// import * as nodemailer from 'nodemailer';
// import Mail from 'nodemailer/lib/mailer';
// import { EmailTemplates } from './EmailTemplates';


/**
 * this class is a Singleton
 */
export class EmailUtil
{
 

  // static readonly FROM_EMAIL:string = "mudanzaselpadrino@gmail.com";
  // static readonly FROM_PASS:string = 'pajuznshspsayqya';//"/admin11"; // app password pavulzavala "lvzmkbzxxpncopjo"; //

  //   static instance:EmailUtil;

  //   transporter:Mail;
  //   // r mailOptions = { 
  //   //   from : '"Fred Foo 👻" pavulzavala@gmail.com', //este aparece en el cuerpo del correo
  //   //   to : 'pavulzavala@gmail.com', //bar@example.com, baz@example.com it can be array
  //   //   subject : 'email test from typescript', 
  //   //   text: 'Hello from typescript',
  //   //   // html: "<b>Hello world?</b>", // html body
  //   // }; 

  //   static getInstance():EmailUtil
  //   {
  //     if(!this.instance)
  //       this.instance = new EmailUtil();

  //       return this.instance;
  //   }

  //   private constructor()
  //   {
  //     this.transporter = nodemailer.createTransport({
  //       host: "smtp.gmail.com",
  //       port: 587,
  //       secure: false, // true for 465, false for other ports
  //       auth: {
  //         user: EmailUtil.FROM_EMAIL, // generated ethereal user
  //         pass: EmailUtil.FROM_PASS //"lvzmkbzxxpncopjo" //this is app password
  //       }}); 
  //   }

  //   /**
  //    * 
  //    */
  //   async sendVerifyEmail(fromEmail:string, verifysecret:string, ...toEmail:string[])
  //   {
  //     console.log("::: ", "verify email sent...")
  //         var mailOptions = { 
  //           from : `"Bitless Services" ${fromEmail}`, //este aparece en el cuerpo del correo
  //           to : `${toEmail}`, //bar@example.com, baz@example.com it can be array
  //           subject : 'Bitless Email Verification', 
  //           text: `click this link to verify your email`,
  //           html: `Please click below to verify your email <br /><a href='http://localhost:8080/access/verifyemail?verifysecret=${verifysecret}'>verify email</a>`, // html body
  //         }; 

  //         this.sendEmail(mailOptions);
  //   }

  //   /**
  //    * this will send the email with the code when the player is trying to authenticate
  //    * to a game, this code must be entered by the player so he can get the token
  //    * @param fromEmail 
  //    * @param verifysecret 
  //    * @param toEmail 
  //    */
  //   async sendGameLoginCodeEmail( fromEmail:string, gameLoginCode:number, ...toEmail:string[] )
  //   {
  //     console.log("::: ", "game login code email sent...")
  //     var mailOptions = { 
  //       from : `"Bitless Services" ${fromEmail}`, //este aparece en el cuerpo del correo
  //       to : `${toEmail}`, //bar@example.com, baz@example.com it can be array
  //       subject : 'Bitless Game Login Code', 
  //       text: `Game Login Code`,
  //       html: `Please copy below code and enter that in the game so you can log in...<br /><b>${gameLoginCode}</b>`, // html body
  //     }; 

  //     this.sendEmail( mailOptions);
  //   }

  //   /**
  //    * this will send an email to force the user to update
  //    * the password
  //    */
  //   async updatePassword()
  //   {}

  //   /**
  //    * this will send an email with the six digints code for
  //    * gameauth 
  //    */
  //   async sendGameAuthCode()
  //   {}


  //   /**
  //    * this will use transporter to send email
  //    * @param mailOptionss 
  //    */
  //   private async sendEmail( mailOptionss:any ):Promise<void>
  //   {
  //     this.transporter.sendMail( mailOptionss, (error, info) => 
  //     { 
  //       if (error) { 
  //         return console.log(`error: ${error}`); 
  //       } 
  //       console.log(`Message Sent ${info.response}`); 
  //     }); 
  //   }

  // /**
  //  * this will send quote email to moving system to know that a potential customer
  //  * wants a quote
  //  * @param mailOptionss 
  //  * @returns 
  //  */
  //   async sendQuoteNotificationEmail(toEmail:string[], htmlBodyContent?:any):Promise<boolean>
  //   {

  //     const mailOptions = { 
  //       from : `"El Padrino Mudanzas" ${EmailUtil.FROM_EMAIL}`, //este aparece en el cuerpo del correo
  //       to : `${toEmail}`, //bar@example.com, baz@example.com it can be array
  //       subject : 'Solicitud de Cotizacion', 
  //       text: `Se ha recibido una cotizacion`,
  //       html: EmailTemplates.getQuoteNotificationTemplate(htmlBodyContent),
  //       attachments: 
  //       [{
  //         filename: 'logored.png',
  //         path:  './public/img/logored.png',
  //         cid: 'logoRED' 
  //       }]
  //     }; 

  //     this.sendEmail(mailOptions);

  //     return true;
  //   }


  //   /**
  //    * this method will send an email to the person that requested the quote
  //    */
  //   async sendResponseQuoteEmail(toEmail:string[],htmlBodyContent?:any)
  //   {

  //     console.log("::: DIR:", __dirname+'/src/util/logored.png');
  //     const mailOptions = { 
  //       from : `"El Padrino Mudanzas" ${EmailUtil.FROM_EMAIL}`, //este aparece en el cuerpo del correo
  //       to : `${toEmail}`, //bar@example.com, baz@example.com it can be array
  //       subject : 'Cotizacion Recibida', 
  //       text: `Se ha recibido una cotizacion`,
  //       html: EmailTemplates.getQuoteResponseTemplate(htmlBodyContent),
  //       attachments: 
  //       [{
  //         filename: 'logored.png',
  //         path:  './public/img/logored.png',
  //         cid: 'logoRED' 
  //       }]
  //     }; 

  //     this.sendEmail(mailOptions);

  //     return true;
  //   }

  //   //***  hacer una funciona para enviar la notificacion al cliente que se hizo una cotizacion y que debe esperar */

}