import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as session from 'express-session';
import { join } from 'path';
import { AppModule } from './app.module';
import * as hbs from "hbs"; 


//this file initiates the program
async function startApp() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

// app.use(
//   session({
//     secret:'texturepackerts',
//     resave:false,
//     saveUninitialized:false
//   })
// )

app.useStaticAssets( join( __dirname, '..', '../public' ) );
app.useStaticAssets( join( __dirname, '..', '../public/css' ) );
app.useStaticAssets( join( __dirname, '..', '../public/ajax' ) );
app.useStaticAssets( join( __dirname, '..', '../public/fonts' ) );
app.useStaticAssets( join( __dirname, '..', '../public/img' ) );
app.useStaticAssets( join( __dirname, '..', '../public/js' ) );
app.useStaticAssets( join( __dirname, '..', '../public/plugins' ) );

app.setBaseViewsDir( [join( __dirname, '..', '../views' ), join( __dirname, '..', '../views/templates' )] );
app.setViewEngine('hbs');

hbs.registerPartials(join(__dirname, '..', '../views/partials'));

//this is a helper to compare if two strings are equal
hbs.registerHelper('compString', function(str1, str2, options) {
  return (str1 === str2) ? options.fn(this) : options.inverse(this);
});
// HandleBars .registerPartial(""); //   compile("");

const port:number =3001; 
console.log("starting app in port: ", port);
  await app.listen( port );
}

startApp();
